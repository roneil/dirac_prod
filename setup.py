from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='dirac_prod',
    use_scm_version=True,
    description='Scripts for testing and running analysis productions in LHCb',
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://gitlab.cern.ch/cburr/dirac_prod',

    author='LHCb',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        # Python 3 needs DIRAC to support it... :'(
    ],
    keywords='LHCb CERN',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    python_requires='>=2.7, <3',
    setup_requires=['setuptools_scm'],
    install_requires=[
        'DIRAC',
        'LHCbDIRAC',
        'six',
        'strictyaml >=1.3.0',
    ],
    entry_points={
        "console_scripts": [
            "dirac-submit-production = dirac_prod.cli:parse_args"
        ],
    },
    project_urls={
        'Bug Reports': 'https://gitlab.cern.ch/cburr/dirac_prod/issues',
        'Source': 'https://gitlab.cern.ch/cburr/dirac_prod',
    },
)
