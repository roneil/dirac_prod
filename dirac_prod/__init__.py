from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__all__ = [
    'classes',
    'exceptions',
    'utils',
]


def handle_signal(sig_id, frame):
    raise KeyboardInterrupt()


import signal
# import sys

# # Force verbose debugging
# existing_argv = sys.argv
# sys.argv = existing_argv[:1] + ['-ddd']
import DIRAC.Core.Base.Script
DIRAC.Core.Base.Script.initialize(script=False)
# sys.argv = existing_argv

# TODO: Deal with these
for sig in [signal.SIGINT, signal.SIGQUIT, signal.SIGTERM]:
    signal.signal(sig, handle_signal)

from DIRAC import gConfig
print("Running inside", gConfig.getValue("/DIRAC/Setup"))

from . import classes
from . import cli
from . import exceptions
from . import schema
from . import utils
