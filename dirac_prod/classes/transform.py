from __future__ import absolute_import
from __future__ import print_function

import json
import xml.etree.ElementTree as ET

from DIRAC.TransformationSystem.Client.TransformationClient import TransformationClient
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

from ..utils import LHCbDIRACObject, dw, ensure_list_int


class Transform(LHCbDIRACObject):
    @classmethod
    def multi_init(cls, ids, allow_missing=False, return_raw=False, _cache=None):
        if not ids:
            return {}
        result, remaining_ids, _cache = super(Transform, cls).multi_init(
            ids, return_raw=return_raw, _cache=_cache
        )
        result.update(cls._search(trans_ids=remaining_ids))
        return cls._check_for_mising_and_return(
            ids, result, return_raw, allow_missing=allow_missing
        )

    @classmethod
    def _search(cls, trans_ids=None, prod_ids=None):
        if bool(trans_ids) + bool(prod_ids) != 1:
            ValueError('One (and only one) of id and raw_data should be given')

        search_params = {}
        if trans_ids is not None:
            search_params['TransformationID'] = ensure_list_int(trans_ids)
        if prod_ids is not None:
            search_params['TransformationFamily'] = ensure_list_int(prod_ids)

        # response = check(trans_client.getTransformations({
        #     'TransformationID': ensure_list_int(ids)
        # }))
        # getTransformationSummaryWeb is faster than getTransformations

        n_results = 1
        raw_data = []
        while n_results > len(raw_data):
            response, n_results = dw(TransformationClient().getTransformationSummaryWeb(
                search_params, [], len(raw_data), 10000
            ), return_total_records=True)
            raw_data += response

        result = {}
        for value in raw_data:
            result[value['TransformationID']] = value

        return result

    @property
    def id(self):
        return int(self.raw_data['TransformationID'])

    @property
    def prod_id(self):
        return int(self.raw_data['TransformationFamily'])

    @property
    def type(self):
        return self.raw_data['Type']

    @property
    def status(self):
        return self.raw_data['Status']

    @property
    def file_status(self):
        """Given a transformation ID return the status of its files."""
        result = {
            'Total': 0,
            'Processed': 0,
            'Running': 0,
            'Failed': 0,
            # 'Path': 'Empty',
            'Hot': 0,
        }

        # files = check(trans_client.getTransformationFilesSummaryWeb(
        #     {'TransformationID': self.id}, [], 0, 1000000
        # ))
        # path_ = check(ts.getTransformationParameters(self.id, ['DetailedInfo']))
        # path = path_.split("BK Browsing Paths:\n", 1)[1]

        # This shows the interesting values
        result['Running'] = self.raw_data['Jobs_Running']
        result['Failed'] = self.raw_data['Jobs_Failed']
        result['Done'] = self.raw_data['Jobs_Done']
        result['Waiting'] = self.raw_data['Jobs_Waiting']
        result['Files_Processed'] = self.raw_data['Files_Processed']
        result['Hot'] = self.raw_data['Hot']

        return result

    @property
    def output_lfns(self):
        try:
            return self._output_lfns
        except AttributeError:
            client = BookkeepingClient()
            lfns = dw(client.getProductionFiles(self.id, 'ALL'))
            # Remove the log files
            self._output_lfns = {
                lfn: metadata for lfn, metadata in lfns.items()
                if metadata['FileType'] != 'LOG'
            }
            return self._output_lfns

    @property
    def steps(self):
        # FIXME: This should be preinitalised
        step_info_data = ET.fromstring(self.raw_data['Body'])
        return json.loads(
            list(step_info_data.find('.//Parameter[@name="BKProcessingPass"]'))[0].text
            .replace('None', 'null')
            .replace('True', 'true')
            .replace('False', 'false')
            .replace("'", '"')
        )  # ['Step0']['BKStepID']
