from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import six

from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

from ..exceptions import AlreadyRegisteredError
from ..utils import dw


class FileType(object):
    _file_type_cache = {}

    def __init__(self, name, description='', visible=None):
        self._name = name
        self._visible = visible
        if self.registered:
            self._description = self.__class__._file_type_cache[self.name]
        else:
            self._description = description

    # Properties
    @property
    def name(self):
        return self._name.upper()

    @property
    def description(self):
        return self._description

    @property
    def visible(self):
        if self._visible is None:
            raise RuntimeError('Visibility has not been set')
        return self._visible

    @property
    def registered(self):
        if self.name in self.__class__._file_type_cache:
            return True
        print(self.name, 'not found in cache')
        self.__class__._file_type_cache = {
            d['FileType']: d['Description']
            for d in dw(BookkeepingClient().getAvailableFileTypes())
        }
        return self.name in self.__class__._file_type_cache

    # Public methods
    def register(self):
        if self.registered:
            raise AlreadyRegisteredError()
        if not isinstance(self.name, six.string_types):
            raise TypeError('name must be a string type')
        if not isinstance(self.description, six.string_types):
            raise TypeError('description must be a string type')
        dw(BookkeepingClient().insertFileTypes(self.name, self.description, 'ROOT'))

    # Private methods
    def __hash__(self):
        if self._visible is None:
            return hash((self.__class__, self.name))
        else:
            return hash((self.__class__, frozenset(self._to_dict().items())))

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self._visible is None:
            return self.name == other.name
        else:
            return self.name == other.name and self.visible == other.visible

    # Required for Python 2.7 compatibility
    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        if self._visible is None:
            return 'FileType(name=%r, description=%r)' % (
                self.name, self.description)
        else:
            return 'FileType(name=%r, description=%r, visible=%r)' % (
                self.name, self.description, self.visible)

    def _to_dict(self):
        """Returns a dictionary suitable for use with step creation"""
        return {
            'FileType': self.name,
            'Visible': self.visible,
        }
