from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import six

from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.DataManagementSystem.Utilities.DMSHelpers import DMSHelpers
from DIRAC.Resources.Storage.StorageElement import StorageElement
from LHCbDIRAC.BookkeepingSystem.Client.BKQuery import BKQuery
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

from dirac_prod.exceptions import DiracError
from dirac_prod.utils import dw, parse_flag


class LFN(object):
    __slots__ = ("_value",)

    def __init__(self, value):
        self._value = value

    def __str__(self):
        return str(self._value)

    def __repr__(self):
        return repr(self._value)

    @property
    def replicas(self):
        replicas = []
        for se, pfn in dw(Dirac().getReplicas([str(self)], active=True))[str(self)].items():
            if DMSHelpers().isSEForJobs(se):
                replicas.append(pfn)
                # try:
                #     replicas.append(dw(StorageElement(se).getURL(self, protocol='root'))[self])
                # except DiracError:
                #     pass
        return replicas


class BookkeepingLFN(LFN):
    __slots__ = ("_file_size", "_event_stat", "_visibility_flag", "_got_replica")

    def __init__(self, lfn, metadata_dict=None):
        super(BookkeepingLFN, self).__init__(lfn)

        if metadata_dict is None:
            metadata_dict = dw(BookkeepingClient().getFileMetadata([str(self)]))[str(self)]
        self._file_size = metadata_dict["FileSize"]
        self._event_stat = metadata_dict["EventStat"]
        self._visibility_flag = metadata_dict["VisibilityFlag"]
        self._got_replica = metadata_dict["GotReplica"]

    @property
    def size(self):
        return self._file_size

    @property
    def n_events(self):
        return self._event_stat

    @property
    def visible(self):
        return parse_flag(self._visibility_flag)

    @property
    def has_replica(self):
        return parse_flag(self._got_replica)


class InputDataset(six.text_type):
    _required_fields = [
        'ConditionDescription',
        'ConfigName',
        'ConfigVersion',
        'DataQuality',
        'EventType',
        'FileType',
        'ProcessingPass',
    ]

    def __init__(self, bk_path=None, transform_ids='ALL', dq_flag='OK', visible=True):
        if bk_path is None:
            raise NotImplementedError('Add support for loading conditions from existing transforms?')
        # TODO Check LFNs exist?
        self._transform_id = transform_ids
        self._bk_query = BKQuery()
        self._bk_query.buildBKQuery(bk_path, prods=transform_ids, visible=visible)
        self._bk_query.setDQFlag()
        query_dict = self._bk_query.getQueryDict()
        for field in self._required_fields:
            if field not in query_dict:
                raise ValueError('Expected %s in bookkeeping path')

    @property
    def lfns(self):
        lfns = self._bk_query.getLFNs(printOutput=False)
        lfn_metadata = dw(BookkeepingClient().getFileMetadata(lfns))
        result = [BookkeepingLFN(lfn, metadata_dict=meta) for lfn, meta in lfn_metadata.items()]
        return result

    @property
    def size(self):
        return self._bk_query.getLFNSize()

    @property
    def event_type(self):
        return self._bk_query.getQueryDict()['EventType']

    @property
    def conditions_description(self):
        return self._bk_query.getQueryDict()['ConditionDescription']

    @property
    def conditions_id(self):
        from LHCbDIRAC.BookkeepingSystem.Client.LHCB_BKKDBClient import LHCB_BKKDBClient

        conditions_path = '/'.join(self.split('/')[:4])
        for node in LHCB_BKKDBClient(web=True).list('/'.join(self.split('/')[:3])):
            if node['fullpath'] == conditions_path:
                break
        else:
            raise RuntimeError('Failed to find conditions path')

        if 'DaqperiodId' in node:
            return node['DaqperiodId']
        elif 'SimId' in node:
            return node['SimId']
        else:
            raise ValueError('Error parsing node: %s for %s' % (repr(dict(node)), self))

    @property
    def conditions_dict(self):
        """Dictionary of conditions, suitable for use as Production input"""
        query_dict = self._bk_query.getQueryDict()
        result = {
            'configName': query_dict['ConfigName'],
            'configVersion': query_dict['ConfigVersion'],
            'inFileType': query_dict['FileType'],
            'inProPass': query_dict['ProcessingPass'][1:],
            'inDataQualityFlag': query_dict['DataQuality'],
            'inProductionID': 'ALL' or ','.join(query_dict['']),
            'inTCKs': 'ALL',
        }

        if 'Production' not in query_dict:
            result['inProductionID'] = 'ALL'
        elif isinstance(query_dict['Production'], six.string_types):
            result['inProductionID'] = query_dict['Production']
        else:
            result['inProductionID'] = ','.join(query_dict['Production'])

        return {k: six.text_type(v) for k, v in result.items()}

    def __str__(self):
        return self._bk_query.makePath()


#  {'BeamCond',
#   'BeamEnergy',
#   'DetectorCond',
#   'G4settings',
#   'Generator',
#   'Luminosity',
#   'MagneticField'},

#  {'condType',
#   'configName',
#   'configVersion',

#   'inDataQualityFlag',
#   'inFileType',
#   'inProPass',
#   'inProductionID',
#   'inTCKs',
#   'simDesc'}


# dw(BookkeepingClient().getSimulationConditions({'SimId': 429376}))[0]
# [{
#     'BeamCond': 'beta* = 2 m, crossingAngle = +0.270 millirad(internal)',
#     'BeamEnergy': '3500 GeV',
#     'DetectorCond': 'Velo Closed',
#     'G4settings': ' ',
#     'Generator': 'Pythia',
#     'Luminosity': 'pp collisions nu = 1, no spillover',
#     'MagneticField': '+1',

#     'SimDescription': 'Beam3500GeV-VeloClosed-MagUp-Nu1',
#     'SimId': 429376,
#     'Visible': 'Y'
# }]

# a = [x for x in LHCB_BKKDBClient(web=True).list('/'.join(InputDataset(bk_path).split('/')[:3])) if x['fullpath'] == '/'.join(InputDataset(bk_path).split('/')[:4])][0]
# b.update(b.pop('selection'))
# {'BeamCondition': 'beta*~3m, zpv=3.0mm, xAngle=-0.105mrad and yAngle=0',
#  'BeamEnergy': '6500 GeV',
#  'ConfigName': 'MC',
#  'ConfigVersion': '2015',
#  'Description': 'Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8',
#  'DetectorCondition': '2015, Velo closed around average x=0.778mm and y=0.131mm',
#  'G4settings': 'specified in sim step',
#  'Generator': 'Pythia8',
#  'Luminosity': 'pp collisions nu = 1.6, 25ns spillover',
#  'MagneticField': '1',
#  'SimId': 433501,
#  'expandable': True,
#  'fullpath': u'/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8',
#  'level': 'Simulation Conditions/DataTaking',
#  'method': 'getConditions',
#  'name': 'Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8'}
