from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import six
import re

from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient

from dirac_prod.classes import FileType as DiracFileType
from dirac_prod.schema import RE_APPLICATION
from .file_type import FileType
from ..utils import dw, TypedProperty, ensure_set, ensure_list, ensure_value
from ..exceptions import AlreadyRegisteredError, NotRegisteredError


STEP_NAME_MAPPING = {
    'ApplicationName': 'App',
    'ApplicationVersion': 'Ver',
    'CONDDB': 'CDb',
    'DDDB': 'DDDb',
    'DQTag': 'DQT',
    'ExtraPackages': 'EP',
    'OptionFiles': 'Opt',
    'OptionsFormat': 'OptF',
    'ProcessingPass': 'Pass',
    'StepId': 'Step',
    'StepName': 'Name',
    'SystemConfig': 'SConf',
    'Usable': 'Use',
    'Visible': 'Vis',
    'isMulticore': 'IsM',
    'mcTCK': 'mcTCK',
    # 'textInputFileTypes': 'IFT',
    # 'textOutputFileTypes': 'OFT',
    # 'textRuntimeProjects': 'TRP'
}


class Step(object):
    @classmethod
    def from_yaml(cls, **kwargs):
        self = cls()

        self.name = kwargs["name"]
        self.processing_pass = kwargs["processing_path"]
        match = re.match(RE_APPLICATION, kwargs["application"])
        if match:
            self.application, self.application_version = match.groups()
        else:
            self.application = ""
            self.application_version = ""
        self.conddb_tag = kwargs["conddb_tag"]
        self.dddb_tag = kwargs["dddb_tag"]
        self.options = kwargs["options"]
        self.options_format = kwargs.get("opt_fmt")
        self.extra_packages = ";".join(kwargs["data_pkgs"])
        self.visible = kwargs["visible"]

        self.ready = True
        # Avoid littering the list of usable steps in the step manager
        self.obsolete = True

        self.input_file_types = [
            DiracFileType(str(fn), visible="N") for fn in kwargs["input_types"]
        ]
        self.output_file_types = [
            DiracFileType(str(fn), visible="N") for fn in kwargs["output_types"]
        ]

        # Check if the step is already known to DIRAC
        self.check_registered()
        return self

    def __init__(self, id=None):
        if id is None:
            self._id = None
            self._name = ''
            self._processing_pass = ''
            self._application = ''
            self._application_version = ''
            self._options = []
            self._options_format = ''
            self._extra_packages = []

            self._conddb_tag = None
            self._dddb_tag = None
            self._system_config = None
            self._ready = False
            self._obsolete = False
            self._visible = True
            self._multicore = False
            self._mc_tck = None
            self._dq_tag = None

            self._input_file_types = []
            self._output_file_types = []
        else:
            self._id = id
            response = dw(BookkeepingClient().getAvailableSteps({'StepId': self.id}))
            if len(response) == 0:
                raise KeyError('Step with id=%i not found' % self.id)
            elif len(response) != 1:
                raise NotImplementedError('Multiple steps found with same id!?')
            self._from_dict(response[0])

    # Required properties
    @TypedProperty(ensure_value, str)
    def name(self):
        pass

    @TypedProperty(ensure_value, str)
    def processing_pass(self):
        pass

    @TypedProperty(ensure_value, str)
    def application(self):
        pass

    @TypedProperty(ensure_value, str)
    def application_version(self):
        pass

    @TypedProperty(ensure_list, str)
    def options(self, options):
        pass

    @TypedProperty(ensure_set, FileType, delayed_load=lambda self: {
        FileType(d['FileType'], visible=d['Visible'])
        for d in dw(BookkeepingClient().getStepInputFiles(self.id))
    })
    def input_file_types(self):
        pass

    @TypedProperty(ensure_set, FileType, delayed_load=lambda self: {
        FileType(d['FileType'], visible=d['Visible'])
        for d in dw(BookkeepingClient().getStepOutputFiles(self.id))
    })
    def output_file_types(self):
        pass

    # Optional properties
    @TypedProperty(ensure_set, str)
    def extra_packages(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def conddb_tag(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def dddb_tag(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def system_config(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def mc_tck(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def options_format(self):
        pass

    @TypedProperty(ensure_value, str, allow_none=True)
    def dq_tag(self):
        pass

    @TypedProperty(ensure_value, bool)
    def multicore(self):
        pass

    @TypedProperty(ensure_value, bool)
    def ready(self):
        pass

    @TypedProperty(ensure_value, bool)
    def obsolete(self):
        pass

    @TypedProperty(ensure_value, bool)
    def visible(self):
        pass

    # Other properties
    @TypedProperty(ensure_value, int, allow_none=True)
    def id(self):
        if self._id is None:
            raise NotRegisteredError()
        return self._id

    @property
    def registered(self):
        return self._id is not None

    def check_registered(self):
        # Don't query on NULL keys as the bookkeeping gets confused
        query = {k: v for k, v in self._to_dict().items() if v}
        matches = dw(BookkeepingClient().getAvailableSteps(query))
        matched = []
        for match in matches:
            match = Step(match['StepId'])
            if self.input_file_types != match.input_file_types:
                continue
            if self.output_file_types != match.output_file_types:
                continue
            matched.append(match)

        if len(matched) == 0:
            return False

        # Sanity check
        actual = matched[0]._to_dict()
        expected = self._to_dict()
        for key in set(list(actual) + list(expected)):
            if key == "StepId":
                continue

            actual_value = actual[key]
            if actual_value == "NULL":
                actual_value = ""
            expected_value = expected[key]

            if actual_value:
                if actual_value == expected_value:
                    continue
            else:
                if not actual_value and not expected_value:
                    continue
            raise NotImplementedError(key, actual_value, expected_value, matched)

        self._id = matched[0].id
        return True

    # Public methods
    def register(self):
        if self.registered:
            raise AlreadyRegisteredError(self)

        for file_type in self.input_file_types.union(self.output_file_types):
            if not file_type.registered:
                file_type.register()

        step_info = self._to_step_manager_dict()
        print('Running insertStep with', step_info)
        self._id = dw(BookkeepingClient().insertStep(step_info))

    # Private methods
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        is_eq = self._to_dict() == other._to_dict()
        is_eq &= self.input_file_types == other.input_file_types
        is_eq &= self.output_file_types == other.output_file_types
        return is_eq

    # Required for Python 2.7 compatibility
    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        try:
            return 'Step(id=%r)' % self.id
        except NotRegisteredError:
            return 'Step(<unregistered>)' % self.id

    def _from_dict(self, d):
        # Set the ID as None to avoid triggering AlreadyRegisteredError
        self._id = None

        self._name = d.get("StepName")
        self._processing_pass = d.get("ProcessingPass")
        self._application = d.get("ApplicationName")
        self._application_version = d.get("ApplicationVersion")
        self._options = (d.get("OptionFiles") or '').split(';')
        self._options_format = d.get('OptionsFormat')
        self._extra_packages = (d.get("ExtraPackages") or '').split(';')

        self._conddb_tag = d.get('CONDDB')
        self._dddb_tag = d.get('DDDB')
        self._system_config = d.get('SystemConfig')
        if "Usable" not in d:
            self._ready = None
            self._obsolete = None
        elif d['Usable'] == 'Not ready':
            self._ready = False
            self._obsolete = False
        elif d['Usable'] == 'Yes':
            self._ready = True
            self._obsolete = False
        elif d['Usable'] == 'Obsolete':
            self._ready = True
            self._obsolete = True
        else:
            pass
            # raise NotImplementedError('Unrecognised value of Usable', d)
        self._visible = {'Y': True, 'N': False, None: False}[d.get('Visible')]
        self._multicore = {'Y': True, 'N': False}[d.get('isMulticore', "N")]
        self._mc_tck = d.get('mcTCK')
        self._dq_tag = d.get('DQTag')

        self._input_file_types = None
        self._output_file_types = None

        if "StepId" in d:
            self._id = int(d['StepId'])

    def _to_dict(self):
        step = {
            'ApplicationName': self.application,
            'ApplicationVersion': self.application_version,
            'ExtraPackages': ';'.join(sorted(self.extra_packages)),
            'OptionFiles': ';'.join(self.options),
            # TODO: What is this?
            'ProcessingPass': self.processing_pass,
            'StepName': self.name,
            'CONDDB': self.conddb_tag or '',
            'DDDB': self.dddb_tag or '',
            'isMulticore': 'Y' if self.multicore else 'N',
            'Visible': 'Y' if self.visible else 'N',  # TODO: Really old steps have Visible = None?
            # TODO: Appears to have never been used
            # TODO: Required for running local tests
            'mcTCK': self.mc_tck or '',
            'DQTag': self.dq_tag or '',
            # TODO: What is RuntimeProjects?
        }
        if self.options_format:
            step['OptionsFormat'] = self.options_format
        # TODO: Required for running local tests
        step['SystemConfig'] = self.system_config or ''
        if self._id is not None:
            step['StepId'] = self.id

        if self.obsolete:
            step['Usable'] = 'Obsolete'
        elif self.ready:
            step['Usable'] = 'Yes'
        else:
            step['Usable'] = 'Not ready'

        return step

    def _to_step_manager_dict(self):
        result = {'Step': self._to_dict()}
        if self.input_file_types:
            result['InputFileTypes'] = [t._to_dict() for t in self.input_file_types]
        if self.output_file_types:
            result['OutputFileTypes'] = [t._to_dict() for t in self.output_file_types]
        else:
            raise NotImplementedError()
        return result

    def _to_production_manager_dict(self, i):
        detail = {}
        if self.input_file_types:
            detail['p' + str(i) + 'IFT'] = ','.join([f.name for f in self.input_file_types])
        if self.output_file_types:
            detail['p' + str(i) + 'OFT'] = ','.join([f.name for f in self.output_file_types])
        for key, value in self._to_dict().items():
            if key not in STEP_NAME_MAPPING:
                print('Step._to_production_dict: Skipping unknown key', key)
                # If the key is missing from step_name_mapping just continue
                continue

            full_key = 'p' + str(i) + STEP_NAME_MAPPING[key]
            if value is None:
                if key in ['SConf']:
                    detail[full_key] = value
            else:
                detail[full_key] = six.text_type(value)

        # detail['p' + str(i) + 'Html'] = 'To be removed'
        kwargs = dict(i=i, ift=detail.get('p%iIFT' % i, ""),
                      oft=detail['p%iOFT' % i], textRuntimeProjects='',
                      SystemConfig="", mcTCK="", OptionsFormat="", DQTag="",
                      StepId="None")
        kwargs.update({k: v or '' for k, v in self._to_dict().items()})
        detail['p' + str(i) + 'Html'] = (
            '<b>Step {i}</b> '
            '{StepName}({StepId}/{ProcessingPass}) : {ApplicationName}-{ApplicationVersion}<br/>'
            'System config: {SystemConfig} MC TCK: {mcTCK}<br/>'
            'Options: {OptionFiles} Options format: {OptionsFormat} '
            'Multicore: {isMulticore}<br/>'
            'DDDB: {DDDB} Condition DB: {CONDDB} DQTag: {DQTag}<br/>'
            'Extra: {ExtraPackages} '
            'Runtime projects: {textRuntimeProjects}<br/>'
            'Visible: {Visible} Usable:{Usable}<br/>'
            'Input file types: {ift} '
            'Output file types: {oft}<br/><br/>'
        ).format(**kwargs)

        p_all = self.application + '-' + self.application_version

        if self.application and self.processing_pass and self.visible:
            p_dsc = self.processing_pass
        else:
            p_dsc = None

        return detail, p_all, p_dsc
