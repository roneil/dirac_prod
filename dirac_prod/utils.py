from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import six

from .exceptions import DiracError


class TypedProperty(object):
    def __init__(self, ensure_func, type_class, allow_none=False, delayed_load=None):
        self.ensure_func = ensure_func
        self.type_class = type_class
        self.allow_none = allow_none
        self.delayed_load = delayed_load

    def __call__(self, func=None):
        self.func = func
        self.__doc__ = func.__doc__
        return self

    def __get__(self, obj, objtype=None):
        value = getattr(obj, '_%s' % self.func.__name__)
        if self.delayed_load and value is None:
            value = self.delayed_load(obj)
            setattr(obj, '_%s' % self.func.__name__, value)
        if self.allow_none and value is None:
            return value
        return self.ensure_func(value, self.type_class, self.func.__name__)

    def __set__(self, obj, value):
        if obj.registered:
            raise AttributeError("can't set attribute on a registered %s"
                                 % obj.__class__.__name__)
        if not (self.allow_none and value is None):
            value = self.ensure_func(value, self.type_class, self.func.__name__)
        setattr(obj, '_%s' % self.func.__name__, value)

    def __delete__(self, obj):
        raise AttributeError("can't delete attribute")


def dw(response, return_total_records=False):
    """Wrapper to validate and parse DIRAC API responses"""
    assert response['OK'], response
    result = response['Value']

    if isinstance(result, dict):
        if 'Successful' in result and 'Failed' in result:
            if result['Failed']:
                raise DiracError('Failed for {!r}'.format(result['Failed']))
            result = result['Successful']

        elif all(k in result for k in ['TotalRecords', 'ParameterNames']):
            total_records = result['TotalRecords']
            result = [dict(zip(result['ParameterNames'], r))
                      for r in result.get('Records', [])]
            if return_total_records:
                result = result, total_records

    return result


def ensure_value(obj, cls, arg_name):
    class_set = cls
    class_set = six.string_types if cls == str else class_set
    class_set = six.integer_types if cls == int else class_set
    if not isinstance(obj, class_set):
        raise TypeError('%s must be of type %s, got %r (%r)' % (arg_name, class_set, type(obj), obj))
    return obj


def ensure_set(obj, cls, arg_name):
    class_set = cls
    class_set = six.string_types if cls == str else class_set
    class_set = six.integer_types if cls == int else class_set
    if isinstance(obj, class_set):
        obj = {obj}
    if not isinstance(obj, collections.Iterable):
        raise TypeError('%s must be an iterable of %s objects, got %r (%r)' % (arg_name, class_set, type(obj), obj))
    if not all(isinstance(f, class_set) for f in obj):
        raise TypeError('%s must be an iterable of %s objects, got %r (%r)' % (arg_name, class_set, type(obj), obj))
    return set(obj)


def ensure_list(obj, cls, arg_name):
    class_set = cls
    class_set = six.string_types if cls == str else class_set
    class_set = six.integer_types if cls == int else class_set
    if isinstance(obj, class_set):
        obj = [obj]
    if not isinstance(obj, collections.Iterable):
        raise TypeError('%s must be an iterable of %s objects, got %r (%r)' % (arg_name, class_set, type(obj), obj))
    if not all(isinstance(f, class_set) for f in obj):
        raise TypeError('%s must be an iterable of %s objects, got %r (%r)' % (arg_name, class_set, type(obj), obj))
    return list(obj)


def parse_flag(flag):
    return {
        # Truthy
        'yes': True,
        'y': True,
        # Falsey
        'no': False,
        'n': False,
    }[flag.lower()]


def is_single_key(x):
    return isinstance(x, six.string_types) or not isinstance(x, collections.Iterable)


def ensure_list_int(x):
    if is_single_key(x):
        x = [x]
    x = [int(y) for y in x]
    return x


class LHCbDIRACObject(object):
    def __init__(self, id=None, raw_data=None, state=None, _cache=None):
        if bool(id) + bool(raw_data) + bool(state) != 1:
            ValueError('Exactly one of id, raw_data and state is required')
        if _cache is None:
            _cache = collections.defaultdict(dict)

        if id:
            try:
                raw_data = _cache[self.__class__][int(id)]
            except Exception:
                raw_data = self.multi_init(id, return_raw=True, _cache=_cache)
                _cache[self.__class__][int(id)] = state

        if state is None:
            state = {'raw_data': raw_data}

        assert isinstance(state, dict)
        self.state = state
        # FIXME: This might be needed but also costs a lot!
        # self.state = deepcopy(state)

    @classmethod
    def multi_init(cls, ids, return_raw=False, _cache=None):
        if _cache is None:
            _cache = collections.defaultdict(dict)

        result = {}
        remaining_ids = set()
        for i in ensure_list_int(ids):
            try:
                result[i] = _cache[cls][i]
            except KeyError:
                remaining_ids.add(i)

        return result, remaining_ids, _cache

    def __repr__(self):
        return '{name}(id={id})'.format(
            name=self.__class__.__name__,
            id=self.id
        )

    def __eq__(self, other):
        if not isinstance(self, other):
            return False
        else:
            return self.id == other.id

    # Required for Python 2.7 compatibility
    def __ne__(self, other):
        return not self == other

    @classmethod
    def _check_for_mising_and_return(cls, key, result, return_raw, allow_missing):
        failed_ids = set()
        for id_ in ensure_list_int(key):
            if id_ not in result:
                failed_ids.add(id_)
        if failed_ids and not allow_missing:
            raise KeyError(failed_ids)

        if not return_raw:
            result = {k: cls(raw_data=v) for k, v in result.items()}

        if is_single_key(key):
            return result[int(key)]
        else:
            return result

    @property
    def raw_data(self):
        return self.state['raw_data']
